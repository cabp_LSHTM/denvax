## Version 0.1.3 submission

This update optionally provides a series of `ggplot2::scale_...` wrappers, refactors the approach to leveraging `data.table` capabilities when available, and adds a `shiny` app the installed extensions.

Re notes: on `zzz.R` - I am aware of the best practices, and have attempted to follow them here. My goal is for the library to provide additional performance and capabilities when other packages are available, to gracefully fallback to base R when they are not available, and to alert the user as to what the case is.

Re warning on `inst/extdata/pub/Makefile`: this is not required for library functionality, and the documentation for
the `denvax::build.project(...)` method and in the Makefile itself clearly state it is written for GNU Make and notes the particular GNU Make extension.

## Version 0.1.2 submission Update

Web searching suggests that the Windows issue may transient item, and I made some other minor updates to the project template / publication replication could in `inst/extdata`, so resubmitting.

## Version 0.1.2 submission

*Fixed the version number update.*

This update corrects a bug using the library with `shiny` package

I'm not sure how to resolve the issue with the Windows check:

```
* checking package dependencies ... ERROR
Packages suggested but not available for checking:
  'devtools', 'roxygen2', 'ggplot2', 'cowplot', 'knitr'

VignetteBuilder package required for checking but not installed: 'knitr'

See section 'The DESCRIPTION file' in the 'Writing R Extensions'
manual.
```

The vignettes build locally just fine.

## 21 August, round II

Change author listing from as.person(c(...)) to c(person(...), person(...), ...).  Also add ORCID info.

## 21 August

Final version of publication out, so updating arXiv to doi links.  Also tweaks to author listing.

## 17 July, round II Notes

Adjusted long running examples to have smaller sample sizes.

Please also note that the potential spelling errors are not actual errors.

## 17 July Submission Notes

> Found the following (possibly) invalid DOIs:
> DOI: 10.1098/rsif.2019.0234
>
> Maybe you can use the arXiv <arXiv:1904.00214> instead.

Updated to that.  Once RSI publishes paper, will resubmit with their DOI.

> Please add more small executable examples in your Rd-files.

All exported functions now have `@example` content.

> Please ensure that your functions do not write by default or in your 
> examples/vignettes/tests in the user's home filespace. That is not allow 
> by CRAN policies. Please only write/save files if the user has specified 
> a directory. In your examples/vignettes/tests you can write to tempdir().

I have modified the function `build.project` to not have a default write location. I'm not sure if there were other areas of concern.  The scripts in inst/extdata also write to disk, but must be provided arguments about where to write (when run from command line) or run by hand in GUI with user-defined inputs.

## More Submission Notes

Round II: hadn't updated the compiled documentation re below digitizer link, should be fixed now.

## Submission Notes

> Thanks, we see:

>   Found the following (possibly) invalid URLs:
>     URL: http://arohatgi.info/WebPlotDigitizer/ (moved to https://arohatgi.info/WebPlotDigitizer/)
>       From: man/morrison2010.Rd
>       Status: 403
>       Message: Forbidden

>   Found the following (possibly) invalid DOIs:
>     DOI: 10.1098/rsif.2019.0234
>       From: DESCRIPTION
>       Status: Not Found
>       Message: 404

> Please fix and resubmit.

> Best,
> Uwe Ligges

I have corrected the link.

The DOI is to-be-issued reservation for a recently re-submitted revise-with-minor-comments publication.

## Test environments
* local OS X install, R 3.6.0
* ubuntu 14.04 (on travis-ci), R 3.6.0
* win-builder (devel and release)

## R CMD check results

0 errors | 0 warnings | 1 note

* This is a new release.
