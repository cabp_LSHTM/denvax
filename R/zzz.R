
.onLoad <- function(libname, pkgname) {
  pkg_ns_env <- parent.env(environment())

  if (requireNamespace("data.table", quietly = TRUE)) {
    packageStartupMessage("denvax using data.table methods.")
    assign("constructor", value = data.table::data.table, envir = pkg_ns_env)
    assign("reductor", value = data.table::rbindlist, envir = pkg_ns_env)
  } else packageStartupMessage(
    "denvax using base R methods; install data.table to enable its use."
  )

  if (requireNamespace("ggplot2", quietly = TRUE)) {
    packageStartupMessage("denvax loading ggplot2 extensions.")
    funs <- list(
      scale_x_startAge = function(
        name = "Initial Age for Routine Testing",
        expand = ggplot2::expand_scale(add = 0.5),
        breaks = function(xlim) seq(ceiling(xlim[1]), floor(xlim[2])),
        ...
      ) ggplot2::scale_x_continuous(
        name = name, expand = expand, ...
      ),
      scale_y_endAge = function(
        name = "Maximum Age for Testing",
        expand = ggplot2::expand_scale(add = 0.5),
        ...
      ) ggplot2::scale_y_continuous(
        name = name, expand = expand, ...
      ),
      scale_y_numTests = function(
        name = "Maximum # of Tests",
        breaks = function(lims) seq(round(lims[1]), round(lims[2]), by = 1),
        expand = ggplot2::expand_scale(add = 0.5),
        ...
      ) ggplot2::scale_y_continuous(
        name = name, expand = expand, breaks = breaks, ...
      ),
      scale_x_log10tau = function(
        name = expression(tau * ", Test Cost Fraction (log scale)"),
        labels = function(b) sprintf("%0.2f", 10 ^ b),
        expand = ggplot2::expand_scale(),
        ...
      ) ggplot2::scale_x_continuous(
        name = name, labels = labels, expand = expand, ...
      ),
      scale_y_nu = function(
        name = expression(nu * ", Vaccine Cost Fraction"),
        labels = function(b) sprintf("%0.1f", b),
        expand = ggplot2::expand_scale(),
        ...
      ) ggplot2::scale_y_continuous(
        name = name, labels = labels, expand = expand, ...
      ),
      scale_linetype_ROIlvl = function(
        name = "ROI Limits", drop = FALSE,
        values = c(`0`="dotted", `0.1`="dotdash", `0.25`="longdash",`0.5`="solid"),
        ...
      ) ggplot2::scale_linetype_manual(
        name = name, drop = drop, values = values, ...
      ),
      scale_color_ROIsign = function(
        name = "ROI Direction", breaks = c(1,0,-1), drop = FALSE,
        labels = c(`-1`="Negative",`0`="Neutral",`1`="Positive"),
        values = c(`-1`="firebrick",`0`="black",`1`="dodgerblue"),
        ...
      ) ggplot2::scale_color_manual(
        name = name, breaks, labels = labels, drop = drop, values = values, ...
      ),
      scale_fill_ROI = function(
        name = "ROI", limits = c(-1, 2), breaks = seq(-1, 2, by = 0.5),
        na.value = "#3A3A98", ...
      ) ggplot2::scale_fill_gradient2(
        name = name, limits = limits, breaks = breaks,
        na.value = na.value, ...
      ),
      rescale_x_Tcost = function(
        S, name = "Per Test Cost (USD)",
        labels = function(b) b*S,
        ...
      ) ggplot2::scale_x_continuous(
        name = name, labels = labels, ...
      ),
      rescale_y_Vcost = function(
        S, name = "Vaccine Cost (USD)",
        labels = function(b) b*S,
        ...
      ) ggplot2::scale_y_continuous(
        name = name, labels = labels, ...
      )
    )

    mapply(
      function(name, value, envir) assign(name, value, envir = envir),
      name = names(funs), value = funs, MoreArgs = list(envir = pkg_ns_env),
      SIMPLIFY = FALSE
    )

  } else packageStartupMessage(
    "ggplot2 extensions from denvax not enabled; install ggplot2 to enable."
  )
}
