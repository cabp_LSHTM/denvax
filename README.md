# Overview

This package provides the core computations used in [Serostatus Testing & Dengue Vaccine Cost-Benefit Thresholds](https://doi.org/10.1098/rsif.2019.0234).

If `ggplot2` is present, the package also provides some convenient `scale_...` elements. If not, these functions issue a warning when used.

Finally, the package installs the complete analysis scripts as external data (`inst/extdata`), as well as an [`rshiny` app](https://samclifford.shinyapps.io/Denvax_demo) used to [present these results](https://doi.org/10.6084/m9.figshare.11695155).

# Development Notes

To allow testing the tailored dependencies in `denvax`, we use `renv` for development, with all packages deactivated, since `denvax` has no hard dependencies. In general, development occurs *without* `renv` active. When ready to test the reduced capability version (*i.e.*, after build + install + test with an environment with `ggplot2` and `data.table` installed), run `renv::install("~/path/to/denvax"); renv::activate()` and restart your R session.
