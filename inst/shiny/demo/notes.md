### Notes

This application provides an interactive version of the analysis in [Serostatus Testing & Dengue Vaccine Cost-Benefit Thresholds](https://doi.org/10.1098/rsif.2019.0234).

It is included as part of the [`denvax`](https://cran.r-project.org/package=denvax) package, and the source can be found as part of the [package source](https://gitlab.com/cabp_LSHTM/denvax/-/tree/master/inst/shiny/demo).

### Authors

This application was developed during a hack-a-thon hosted by the [Centre for Mathematical Modelling of Infectious Disease](https://cmmid.lshtm.ac.uk). The following people contributed: A. Richards, Y. Jafari, T. Sumner, B. Savagar, L. Yakob, N. Waterlow, Y. Liu, B. Quilty, J. Villabona Arenas, and D. Simons.

The current version of the application was refined by Sam Clifford and Carl Pearson.
